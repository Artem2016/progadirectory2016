import java.util.Scanner;

public class ABC extends Thread {
    String whoAmI;
    final Object synchoro;
    int a;
    static String next = " A ";



    ABC(String whoami, Object sync, int count) {
        whoAmI = whoami;
        synchoro = sync;
        a = count;
    }


    void dodo() {
        synchronized (synchoro) {

            for (int j = 0; j < a; j++) {

                if (next == whoAmI) {
                    System.out.print(whoAmI);
                    if (whoAmI == " A ") {
                        next = " B ";
                    }
                    else {
                        if (whoAmI == " B ") {
                            next = " C ";
                        }
                        else {
                            next = " A ";
                        }
                    }
                    synchoro.notifyAll();
                    try {
                        synchoro.wait();
                    } catch (Exception e) {
                    }
                }
                else {
                    synchoro.notifyAll();
                    j--;
                    try {
                        synchoro.wait();
                    } catch (Exception e) {
                    }
                }
                synchoro.notifyAll();

            }
        }
    }

    @Override


    public void run() {
        dodo();
    }

    public static void main(String[] args) throws InterruptedException {

        Integer x = 1;

        Scanner s = new Scanner(System.in);
        int a = s.nextInt();


        ABC m = new ABC(" A ", x, a);
        m.start();


        ABC n = new ABC(" B ", x, a);
        n.start();


        ABC c = new ABC(" C ", x, a);
        c.start();

    }
}