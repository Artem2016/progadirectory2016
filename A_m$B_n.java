import java.util.Scanner;

public class A_m$B_n extends Thread {
    String whoAmI;
    final Object synchoro;
    int a;
    int b;
    boolean flag;


    A_m$B_n(String whoami, Object sync, boolean chto, int count1, int count2) {
        whoAmI = whoami;
        synchoro = sync;
        flag = chto;
        a = count1;
        b = count2;
    }


    void dodo() {
        synchronized (synchoro) {


            for (int j = 0; j < Math.min(a, b); j++) {
                System.out.print(whoAmI);
                synchoro.notify();
                try {
                    synchoro.wait();
                } catch (Exception e) {
                }
            }


            for (int i = 0; i < Math.abs(a - b); i++) {
                if (flag == true) {
                    System.out.print(whoAmI);
                }
                synchoro.notify();
                try {
                    synchoro.wait();
                } catch (Exception e) {
                }
            }
            synchoro.notifyAll();
        }
    }


    @Override

    public void run() {
        dodo();
    }

    public static void main(String[] args) throws InterruptedException {

        Integer x = 1;
        Scanner s = new Scanner(System.in);
        int a = s.nextInt();
        int b = s.nextInt();
        boolean flag1;
        boolean flag2;

        if (a >= b) {
            flag1 = true;
            flag2 = false;
        }
        else
        {
            flag1 = false;
            flag2 = true;
        }

        A_m$B_n m = new A_m$B_n(" A ", x, flag1, a, b);
        m.start();


        A_m$B_n n = new A_m$B_n(" B ", x, flag2, a, b);
        try {
            m.join(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        n.start();
    }
}
