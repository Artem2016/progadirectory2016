// Задания : Решение уравнения ax=1 (mod b) , Решение уравнения в общем виде (CL 2016.03.01)
// На вход : (a,b,c) : ax=b (mod c)

import java.util.Scanner;

public class ModDavai {

    public static long delenie(long a, long b) {
        long z = 0;

        while (a >= b) {
            a = a - b;
            z++;
        }

        if (a % b == 0 || a < b) {
            return z;
        } else {
            return z - 1;
        }

    }

    public static long delenie2(long a, long b, long i) {
        if (i == 0) {
            return delenie(a, b);
        } else {
            return delenie2(b, a % b, i - 1);
        }
    }

    public static long delenieNum(long a, long b, long i) {

        if (a % b != 0) {
            return delenieNum(b, a % b, i + 1);
        } else {
            return i+1;
        }
    }

    public static long pognali(long f[][], long a, long b) {

        f[0][0] = 0;
        f[1][0] = 1;

        for (int i = 1; i < f[0].length; i++) {
            f[0][i] = delenie2(a, b, i - 1);

        }
        f[1][1] = f[0][1] * f[1][0];
        for (int j = 2; j < f[0].length; j++) {
            f[1][j] = (f[0][j] * f[1][j - 1]) + f[1][j - 2];
        }
        return f[1][f[0].length-2];
    }

    public static long delitel1(long a, long b) {

        while (a != 0 && b != 0) {

            if (a > b) {
                a = a % b;
            } else {
                b = b % a;
            }
        }
        return a + b;
    }


    public static long ravmod(long l, long k) {
        if (l>=0) {
            while (l >= k) {
                l = l - k;
            }
            return l;
        }
        else {
            while (l <= k)
            {
                l = l + k;
            }
            return l-k;
        }
    }



    public static void sol(long a, long b, long m, long z){
        long d = delitel1(a,m);
        long x;
        long k = delenieNum(m,a,0);
        long mas[][] = new long[2][(int)k+1];

        if (b%d == 0) {
            if (d != 1) {
                sol(a/d,b/d,m/d, d);
            }

            else {
                x = (long)Math.pow(-1,mas[0].length-2) * pognali(mas, m, a) * b;


                for (long i = 0; i < z; i++) {
                    System.out.println("x=" + (ravmod(x, m)+m*i) + " mod(" + m*z + ")");


                }
            }

        }
        else{
            System.out.print("Решений нет!");
        }
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        long a = s.nextLong();
        long b = s.nextLong();
        long c = s.nextLong();

        sol (a,b,c,1);

    }
}
