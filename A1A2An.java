import java.util.Scanner;

public class A1A2An extends Thread {
    String whoAmI;
    final Object synchoro;
    int a;
    int b;
    int c;
    static String next = " A" +String.valueOf(1);



    A1A2An(String whoami, Object sync, int now, int count, int count2) {
        whoAmI = whoami;
        synchoro = sync;
        a = count;
        b = count2;
        c = now;
    }



    void dodo() {
        synchronized (synchoro) {

            for (int j = 0; j < a; j++) {

                if (next.equals(whoAmI)) {
                    System.out.print(whoAmI);
                    if (c < b) {
                        next = " A" + String.valueOf(c + 1);
                    }
                    else {
                        if (c == b) {
                            next = " A" + 1;

                        }
                    }
                    synchoro.notifyAll();
                    try {
                        synchoro.wait();
                    } catch (Exception e) {
                    }
                }
                else {
                    synchoro.notifyAll();
                    j--;
                    try {
                        synchoro.wait();
                    } catch (Exception e) {
                    }
                }
                synchoro.notifyAll();

            }
        }
    }

    @Override


    public void run() {
        dodo();
    }

    public static void main(String[] args) throws InterruptedException {

        Integer x = 1;

        Scanner s = new Scanner(System.in);
        int b = s.nextInt();
        int a = s.nextInt();


        for (int i = 1; i <= b; i++) {
            A1A2An newThread = new A1A2An(" A" +String.valueOf(i), x, i, a, b);
            newThread.start();

        }


    }
}