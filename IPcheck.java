import java.util.Scanner;

public class IPcheck {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String ip;
        ip = sc.nextLine();
        if ((ip.length() - ip.replaceAll("\\.", "").length()) == 3) {

            String[] ipParts = ip.split("\\.");

            try {

                int length1 = ipParts[0].length();
                int length2 = ipParts[1].length();
                int length3 = ipParts[2].length();
                int length4 = ipParts[3].length();


                int v = Integer.parseInt(ipParts[0]);
                int x = Integer.parseInt(ipParts[1]);
                int y = Integer.parseInt(ipParts[2]);
                int z = Integer.parseInt(ipParts[3]);

                if (length1 <= 3 && length2 <= 3 && length3 <= 3 && length4 <= 3) {
                    if (v <= 255 && x <= 255 && y <= 255 && z <= 255) {
                        System.out.print("True");
                    } else {
                        System.out.print("False");
                    }
                } else {
                    System.out.print("False");
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.print("False");
            } catch (NumberFormatException e) {
                System.out.print("False");
            }
        } else {
            System.out.print("False");
        }
    }
}
