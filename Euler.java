import java.util.Scanner;

public class Euler {

    public static long delitel(long a, long b) {

        while (a != 0 && b != 0) {

            if (a > b) {
                a = a % b;
            } else {
                b = b % a;
            }
        }
        return a + b;
    }

    public static long Eulerfunction(long n) {

        long z = 0;

        if (n > 0) {
            for (long i = 1; i < n; i++) {
                if (delitel(n, i) == 1) {
                    z++;
                }
            }
        }
        return z;
    }



    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        long a = s.nextLong();
        System.out.print(Eulerfunction(a));
    }
}


