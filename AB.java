import java.util.Scanner;

public class AB extends Thread {
    String whoAmI;
    final Object synchoro;
    int a;


    AB(String whoami, Object sync, int count) {
        whoAmI = whoami;
        synchoro = sync;
        a = count;
    }



    void dodo() {
        synchronized (synchoro) {


            for (int j = 0; j < a; j++) {
                System.out.print(whoAmI);
                synchoro.notify();
                try {
                    synchoro.wait();
                } catch (Exception e) {
                }
            }
            synchoro.notifyAll();
        }
    }



    @Override

    public void run() {
        dodo();
    }

    public static void main(String[] args) throws InterruptedException {

        Integer x = 1;

        Scanner s = new Scanner(System.in);
        int a = s.nextInt();

        AB m = new AB(" A ", x, a);
        m.start();



        AB n = new AB(" B ", x, a);
        try {
            m.join(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        n.start();


    }
}