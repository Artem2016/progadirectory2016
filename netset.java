import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;

public class netset {

    public static void main( String args[]) throws Exception {

        InetAddress mask = Inet4Address.getLocalHost();
        NetworkInterface networkInterface = NetworkInterface.getByInetAddress(mask);
        String subnetMask= "Маска Подсети: " + sop(networkInterface.getInterfaceAddresses().get(0).getNetworkPrefixLength());
        System.out.println(subnetMask);



        InetAddress IP;
        IP = InetAddress.getLocalHost();
        System.out.println("IP адресс: " + IP.getHostAddress());

    }
    public static String sop(int a) {
        String result = "";
        if (a<=30&&a>=16) {
            if (a == 30) {
                result = "255.255.255.252";
            }
            if (a == 29) {
                result = "255.255.255.248";
            }
            if (a == 28) {
                result = "255.255.255.240";
            }
            if (a == 27) {
                result = "255.255.255.224";
            }
            if (a == 26) {
                result = "255.255.255.192";
            }
            if (a == 25) {
                result = "255.255.255.128";
            }
            if (a == 24) {
                result = "255.255.255.0";
            }
            if (a == 23) {
                result = "255.255.254.0";
            }
            if (a == 22) {
                result = "255.255.252.0";
            }
            if (a == 21) {
                result = "255.255.248.0";
            }
            if (a == 20) {
                result = "255.255.240.0";
            }
            if (a == 19) {
                result = "255.255.224.0";
            }
            if (a == 18) {
                result = "255.255.192.0";
            }
            if (a == 17) {
                result = "255.255.128.0";
            }
            if (a == 16) {
                result = "255.255.0.0";
            }
        } else {
            System.out.print("/" + a);
        }

        return result;
    }
}

