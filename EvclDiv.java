import java.util.Scanner;

public class EvclDiv {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int a = s.nextInt();
        int b = s.nextInt();

        while (a!=0 && b!=0) {
            if (a > b) {
                a = a%b;
            }
            else {
                b = b%a;
            }

        }
        System.out.print(a+b);
    }
}

