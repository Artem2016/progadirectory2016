import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * Created by ArtemPC on 03.06.2016.
 */
public class ARPtable {

    public static String getARPTable(String cmd) throws IOException {
        Scanner s = new Scanner(Runtime.getRuntime().exec(cmd).getInputStream()).useDelimiter("\\A");
        return s.next().replaceAll("����䥩�","Интерфейс").replaceAll("���� � ���୥�", "адрес в Интернете").replaceAll("�����᪨� ����","Физический адрес").replaceAll("�������᪨�","динамический").replaceAll("����᪨�","статический").replaceAll("���","Тип");
    }


    public static void main(String[] args) throws IOException {

        System.out.print(getARPTable("arp -a"));
    }
}
