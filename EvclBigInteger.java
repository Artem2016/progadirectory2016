import java.math.BigInteger;

public class EvclBigInteger {
    public static void main(String[] args) {
        BigInteger k = BigInteger.valueOf(2);
        BigInteger j = BigInteger.valueOf(2);

        BigInteger b = k.pow(564).subtract(BigInteger.ONE);
        BigInteger a = j.pow(525).subtract(BigInteger.ONE);

        while (b.compareTo(BigInteger.ZERO) !=0 && a.compareTo(BigInteger.ZERO) !=0) {
            if (a.compareTo(b) == 1) {
                a = a.remainder(b);
            }
            else {
                b = b.remainder(a);;
            }
        }
        System.out.print(b.add(a));
    }
}
