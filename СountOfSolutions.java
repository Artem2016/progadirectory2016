//1-2 задача из ДЗ на 19.01.2016 || На вход: (количество x , сумма , верхняя граница x)


import java.math.BigInteger;
import java.util.Scanner;

public class СountOfSolutions {


    public static BigInteger factorial(BigInteger n)
    {
        BigInteger z = BigInteger.ONE;
        for (long i = 1; BigInteger.valueOf(i).compareTo(n) != 1; ++i) z = z.multiply(BigInteger.valueOf(i));
        return z;
    }

    public static BigInteger C(BigInteger n, BigInteger k) {

        if (k.compareTo(n) == 1) {
            return BigInteger.ZERO;
        } else {
            return (factorial(n).divide(factorial(k).multiply(factorial(n.subtract(k)))));
        }

    }

    public static BigInteger solution(BigInteger a, BigInteger b) {

        if (b.compareTo(BigInteger.ZERO) == -1 || a.compareTo(BigInteger.ZERO) == -1) {
            return BigInteger.ZERO;
        } else {
            return C((a.add(b)).subtract(BigInteger.ONE), a.subtract(BigInteger.ONE));
        }

    }

    static String completeZeros(String s, int l) {
        String result = s;

        for (int i = 0; i < l - s.length(); i++) {
            result = "0" + result;
        }
        return result;


    }


    public static int[][] zapolnit(int[][] f, int l) {

        for (int i = 0; i < f.length ; i++) {
            for (int j = 0; j < f[i].length  ; j++) {
                f[i][j] = Character.getNumericValue(completeZeros(Integer.toString(i,2),l).charAt(j));

            }
        }
        return f;
    }

    public static int summa(int[][] f, int j) {
        int z = 0;

        for (int i = 0; i < f[j].length ; i++) {
            z = z + f[j][i];
        }

        return z;
    }

    public static BigInteger vkluchenie(int a, long b, long c) {
        BigInteger z = BigInteger.ZERO;

        int[][] test = new int[(int)Math.pow(2, a)][a];
        int[][] test2 =new int[(int)Math.pow(2, a)][a];
        test2 = zapolnit(test, a);

        if (c*a >= b) {

            for (int i = 0; i <= (int)Math.pow(2, a)-1; i++) {

                if (summa(test2, i) % 2 == 1) {

                    z = z.add(solution(BigInteger.valueOf(a), BigInteger.valueOf((b - (c+1) * (summa(test2, i))))));
                    //System.out.println(z);
                } else {

                    if (summa(test2, i) != 0 && summa(test2, i) % 2 == 0) {

                        z = z.subtract(solution(BigInteger.valueOf(a), BigInteger.valueOf((b - (c+1) * (summa(test2, i))))));
                        //System.out.println(z);

                    }


                }

            }
        }

        else {
            z=solution(BigInteger.valueOf(a), BigInteger.valueOf(b));
        }

        return solution(BigInteger.valueOf(a), BigInteger.valueOf(b)).subtract(z);

    }



    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);
        int a = s.nextInt(); //Кол-во
        long b = s.nextLong(); //Cумма
        long c = s.nextLong(); //Граница Верхняя


        System.out.println(vkluchenie(a, b, c));


    }
}

