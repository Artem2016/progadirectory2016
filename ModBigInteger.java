// Задания : Решение уравнения для больших c НОД (DZ 2016.03.15) , Решение уравнения ax=1 (mod b) для больших (CL 2016.03.01)
// На вход : (a,b,c) : ax=b (mod c)

import java.math.BigInteger;
import java.util.Scanner;

public class ModDavaiBigInteger {

    public static BigInteger delenie(BigInteger a, BigInteger b) {
        BigInteger z = BigInteger.ZERO;

        while (a.compareTo(b) != -1) {
            a = a.subtract(b);
            z = z.add(BigInteger.ONE);
        }

        if ((a.remainder(b)).compareTo(BigInteger.ZERO) == 0 || a.compareTo(b) == -1) {
            return z;
        } else {
            return z.subtract(BigInteger.ONE);
        }

    }

    public static BigInteger delenie2(BigInteger a, BigInteger b, BigInteger i) {
        if (i.compareTo(BigInteger.ZERO) == 0) {
            return delenie(a, b);
        } else {
            return delenie2(b, a.remainder(b), i.subtract(BigInteger.ONE));
        }
    }

    public static BigInteger delenieNum(BigInteger a, BigInteger b, BigInteger i) {

        if ((a.remainder(b)).compareTo(BigInteger.ZERO) != 0) {
            return delenieNum(b, a.remainder(b), i.add(BigInteger.ONE));
        } else {
            return i.add(BigInteger.ONE);
        }
    }

    public static BigInteger pognali(BigInteger f[][], BigInteger a, BigInteger b) {

        f[0][1] = BigInteger.ZERO;
        f[1][0] = BigInteger.ONE;

        for (int i = 1; i < f[0].length; i++) {
            f[0][i] = delenie2(a, b, BigInteger.valueOf(i - 1));

        }
        f[1][1] = (f[0][1]).multiply(f[1][0]);
        for (int j = 2; j < f[0].length; j++) {
            f[1][j] = (((f[0][j]).multiply(f[1][j - 1]))).add(f[1][j - 2]);
        }
        return f[1][f[0].length-2];
    }

    public static BigInteger pow(BigInteger base, BigInteger exponent) {
        BigInteger result = BigInteger.ONE;
        while (exponent.signum() > 0) {
            if (exponent.testBit(0)) result = result.multiply(base);
            base = base.multiply(base);
            exponent = exponent.shiftRight(1);
        }
        return result;
    }

    public static BigInteger delitel1(BigInteger a, BigInteger b) {

        while (b.compareTo(BigInteger.ZERO) !=0 && a.compareTo(BigInteger.ZERO) !=0) {
            if (a.compareTo(b) == 1) {
                a = a.remainder(b);
            }
            else {
                b = b.remainder(a);;
            }
        }
        return a.add(b);
    }

    public static BigInteger ravmod(BigInteger l, BigInteger k) {
        if (l.compareTo(BigInteger.ZERO) != -1) {
            while (l.compareTo(k) != -1) {
                l = l.subtract(k);
            }
            return l;
        }
        else {
            while (l.compareTo(k) != 1)
            {
                l = l.add(k);
            }
            return l.subtract(k);
        }
    }



    public static void sol(BigInteger a, BigInteger b, BigInteger m, BigInteger z){
        BigInteger d = delitel1(a,m);
        BigInteger x;
        BigInteger k = delenieNum(m,a,BigInteger.ZERO);
        BigInteger mas[][] = new BigInteger[2][k.intValue()+1];

        if ((b.remainder(d)).compareTo(BigInteger.ZERO) == 0) {
            if (d.compareTo(BigInteger.ONE) != 0) {
                sol((a.divide(d)),(b.divide(d)),(m.divide(d)), (d));
            }

            else {
                x = ((BigInteger.valueOf((long)Math.pow(-1,mas[0].length-2))).multiply(pognali(mas, m, a))).multiply(b);


                for (BigInteger i = BigInteger.ZERO; i.compareTo(z) == -1; i=i.add(BigInteger.ONE)) {
                    System.out.println("x=" + ((ravmod(x, m)).add(m.multiply((i)))) + " mod(" + (m.multiply(z)) + ")");


                }
            }

        }
        else{
            System.out.print("Решений нет!");
        }
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        BigInteger a = s.nextBigInteger();
        BigInteger b = s.nextBigInteger();
        BigInteger c = s.nextBigInteger();

        sol (a,b,c,(BigInteger.ONE));

    }
}
