import java.util.Arrays;
import java.util.Scanner;


public class MulPrimes {

    public static String mulPrime(long f[], long a, int b) {

        if (a > 0) {


            if (a == 1) {

                return "1";

            } else {

                if (f.length > b) {
                    if (a % f[b] == 0) {
                        return String.valueOf(f[b]) + "*" + mulPrime(f, a / f[b], b);
                    } else {
                        return mulPrime(f, a, b + 1);
                    }
                } else {
                    return "1*" + String.valueOf(a);
                }
            }
        }

        else {
            return "x<=0!";
        }
    }



    public static long[] zapoln(long n, long f[]) {
        int currentNumber, dividers;
        int k = 0;
        for (currentNumber = 2; currentNumber < n; currentNumber++) {
            dividers = 0;
            for (int i = 1; i <= currentNumber; i++) {
                if (currentNumber % i == 0)
                    dividers++;

            }
            if (dividers <= 2) {
                f[k] = currentNumber;
                k++;
            }

        }

        return f;
    }

    public static long chislo(long n) {
        int currentNumber, dividers;
        int z = 0;

        for (currentNumber = 2; currentNumber < n; currentNumber++) {
            dividers = 0;
            for (int i = 1; i <= currentNumber; i++) {
                if (currentNumber % i == 0)
                    dividers++;

            }
            if (dividers <= 2) {

                z++;
            }

        }
        return z;
    }


    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        long n = s.nextLong();

        long[] test = new long[(int)chislo(n)];

        //System.out.println(Arrays.toString(zapoln(n,test)));
        System.out.println(mulPrime((zapoln(n,test)),n,0));

    }
}


