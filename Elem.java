import java.io.FileInputStream;
import java.io.IOException;
import java.util.PriorityQueue;

public class Elem implements Comparable<Elem> {
    Elem left;
    Elem right;
    String val;
    int freq;

    public Elem(String Value, int Frequency) {
        val = Value;
        freq = Frequency;
    }

    public Elem(int Frequency) {
        freq = Frequency;
    }

    public int compareTo(Elem t) {
        if (freq > t.freq) {
            return 1;
        } else {
            if (freq < t.freq) {
                return -1;
            } else {
                return 0;
            }
        }
    }

    public static Elem buildTree(int freq[], String text[]) {
        PriorityQueue<Elem> queueoflists = new PriorityQueue<>();
        for (int i = 0; i < text.length; i++) {
            Elem t = new Elem(text[i], freq[i]);
            queueoflists.add(t);
        }
        Elem Tree = null;
        while (queueoflists.size() > 1) {
            Elem list1 = queueoflists.remove();
            Elem list2 = queueoflists.remove();
            Elem NewBranch = new Elem(list1.freq + list2.freq);
            NewBranch.right = list1;
            NewBranch.left = list2;
            queueoflists.add(NewBranch);
            Tree = NewBranch;
        }
        return Tree;
    }

    public static void PrintAlphabet(Elem t, String prefix) {

        if (t.val != null) {
            System.out.println(prefix + " " + t.val);
        }

        if (t.left != null) {
            PrintAlphabet(t.left, prefix + "1");
        }
        if (t.right != null) {
            PrintAlphabet(t.right, prefix + "0");
        }
    }

    public static int SpecLength (int[] n) {
        int z = 0;
        for (int i = 0; i < n.length; i++){
            if (n[i] == 0) {
                z++;
            }
        }
        return n.length-z;
    }


    public static void main(String[] args) throws IOException {

        FileInputStream in = new FileInputStream("test.txt");
        int[] mas1 = new int[256];
        int k = in.available();
        int z =0;
        for (int i = 0; i < k; i++) {
            mas1[in.read()]++;}
        String[] text = new String[SpecLength(mas1)];
        int[] frequencies = new int[SpecLength(mas1)];
        for (int i = 0; i < 256; i++) {
            if (mas1[i] != 0) {
                text[z]= Character.toString((char)(i));
                frequencies[z] =mas1[i];
                z++;
            }
        }

        
        PrintAlphabet(buildTree(frequencies, text), "");
    }
}